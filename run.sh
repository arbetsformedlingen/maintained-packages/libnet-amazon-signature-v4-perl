#!/usr/bin/env bash
set -eEu -o pipefail

echo "enter sign key: "
read -s SIGNKEY
env SIGNKEY="$SIGNKEY" make
