-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: libnet-amazon-signature-v4-perl
Binary: libnet-amazon-signature-v4-perl
Architecture: all
Version: 0.21-1
Maintainer: Per Weijnitz <per.weijnitz@arbetsformedlingen.se>
Homepage: https://metacpan.org/release/Net-Amazon-Signature-V4
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11)
Build-Depends-Indep: perl
Package-List:
 libnet-amazon-signature-v4-perl deb perl optional arch=all
Checksums-Sha1:
 4c8c719a55675fa0d83192732d756f3a268515f9 25023 libnet-amazon-signature-v4-perl_0.21.orig.tar.gz
 3dd3ec2bd7f67ba2af4c6609d947230f06e95760 1412 libnet-amazon-signature-v4-perl_0.21-1.debian.tar.xz
Checksums-Sha256:
 599cb766c055f6c48d362597e7535c902cd6674e4d6ad1ce4cb08e8d06777fd1 25023 libnet-amazon-signature-v4-perl_0.21.orig.tar.gz
 c1cde39a731bbd4317ea6f9c7ea8fdb44b2224059f3f98e46ee1b61d6b1ca706 1412 libnet-amazon-signature-v4-perl_0.21-1.debian.tar.xz
Files:
 0ad67cd3ecfcfc5528dbdf78a91bb33e 25023 libnet-amazon-signature-v4-perl_0.21.orig.tar.gz
 7f659cb6bc63bba4f2ac62ea8b1d5657 1412 libnet-amazon-signature-v4-perl_0.21-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEE6niSkXgqx29tzEorbrTKOWRu0agFAmCIKOsACgkQbrTKOWRu
0ahRCgv/Wvm9wuR1cpYy+2jFEI0XGCUVO7AFj0JotzgqunzqgakT9U/kByRTxBO0
AK59zlwUIXYe+D06FhX3GJagjF6lBsKOx3NsMcuEcgDNsOuUpMY1xS63UL4rYAHu
cNDl3FH3BYA7FEiuYdh+hZHwCl3rfATJsSLdpvvpVb5+skzDu6xO4y/OCvmKWLps
ROlCtOo5wRS6RD0PFzl3dSq4grvF+fi34G8LMNkT4FeV6t19yMQfgmiAnJtZC5Kk
q7RyU4+08UqBHS5jgFQhw3Wu8+bx8nupM58eHh6apseybMAn9aEAtMVLkyrIoQsB
RXi4DcF/vu0I1FIATZmKUElnHPQJ6sKR0eLyHgmyE2w6ZLjKDgzSOLIZN4q45rKQ
qTAAyvtOwqBjDVE6BFxWZi6I521Zq0Qe23F0ns/cNDIUalwbdm+ZX4zxcaMr34El
w+3umSOyFWMMZcVF95lwosTXhswUwRg+xkm04QWRqhUwjM8gcdEAtfwgK8sAHTdm
yCInl03W
=lAb9
-----END PGP SIGNATURE-----
